import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.internal.Throwables;

public class compteBancaireTest {
	
/**
 * Cette classe permet de tester les méthodes de la classe compteBancaire; le choix de 
 * assertTrue revient au fait  montrer  si les contraintes sont vérifiées ou pas.
 * le choix des valeurs est toujours positif grace aux différents tests décrit ci-après
 * 
 */
	
	@Test
	public void decouvert() {
		
		int somInit=100;
		
	compteBancaire compte= new compteBancaire("008A3");
	
		assertTrue(" Dans ce cas vous n'êtes pas a découvert",compte.compteInit(somInit)>=0);

		
			
	}
	
	
	@Test
	public void decouvert1() {
	compteBancaire compte= new compteBancaire("008A3");
	
		assertNotNull("vous n'avez pas de compte ni de solde", compte.consultationSolde("008A3"));

		
			
	}
	
	
	@Test
	public void sommePositive() {
			
	   int credit=5;
	   int solde=10;
	   
	   compteBancaire compte= new compteBancaire("008A3");
		
		assertTrue(compte.consultationCredit(solde, credit)>0);
	}
	
	
	@Test
	public void sommeInfDeb() {
		
		int aDebiter=1;
		int solde=3;
		
		compteBancaire compte= new compteBancaire("008A3");
		
		assertTrue(compte.consultationDebit(aDebiter, solde)>0);
		
	}
	
	
	@Test
	public void sommeInfVir() {
	
	int solde=5;
	int somVir=2;
	String nomCompte="008A3";
	
	compteBancaire compte=new compteBancaire("008A3");
	
	assertTrue(compte.virement(solde, somVir, nomCompte)>0);
	}

}
